<image-list>
  <div class="">
    <table class="table table-bordered">
      <tbody>
          <tr each={ data,idx in images }>
              <td class="text-center align-middle"><canvas id="preview_{ idx }" class="border border-dark" width="{ preview_canvas_w }px" height="{ preview_canvas_h }px"></canvas><p class="img_name"></p></td>
              <td><canvas id="canvas_{ idx }"></canvas></td>
          </tr>
      </tbody>
    </table>
  </div>
  <script>
	const settings = require('./setting.js');
	let mode;
  const fs = require('fs');
  const _path = require('path');
  var getDirName = _path.dirname;
  const mkdirp = require("mkdirp");
  const dt = new Date();
  const dest = `result_${dt.getFullYear()}${format_zero(dt.getMonth()+1)}${format_zero(dt.getDate())}${format_zero(dt.getHours())}${format_zero(dt.getMinutes())}${format_zero(dt.getSeconds())}`;
	var dest_dir = '';
  let d_w = 180;
  let d_h = 240;
	let canvas_max_w = 600;
	var $progressbar = $('#progressbar');
	var create_count = 0;
  var _this = this;
  const prompt = require('electron-prompt');
  const {dialog} = require('electron').remote;

	this.images = [];
	this.preview_canvas_w;
	this.preview_canvas_h;
  obs.on('getPath', function(list) {
		$('.function_btn').attr('disabled', true);
		mode = settings[$('select[name=mode]').val()];
		canvas_max_w = mode.canvas_max_w || canvas_max_w;
		d_w = mode.d_w || d_w;
		d_h = mode.d_h || d_h;
    
		_this.images = list;
		_this.preview_canvas_w = d_w;
		_this.preview_canvas_h = d_h;
		_this.update();
		dest_dir = getDirName(list[0]);
		$("#denominator").text(list.length);
		$("#numerator").text(0);
		crop(0);
		
  });

	function crop (i) {
		let canvas = document.getElementById(`canvas_${i}`);
		let context = canvas.getContext('2d');
		let image = new Image();
		image.onload = function(e) {
			let canvas_w = Math.min(e.path[0].width, canvas_max_w);
			let canvas_h = canvas_w*e.path[0].height/e.path[0].width;
			let scale = e.path[0].width/canvas_w;

			canvas.setAttribute('width', canvas_w);
			canvas.setAttribute('height', canvas_h);
			context.drawImage(image, 0, 0, e.path[0].width, e.path[0].height, 0, 0, canvas_w, canvas_h);

      const st = Math.max($(canvas).offset().top-(canvas_h/2), 0);
      $('html,body').animate({scrollTop: st}, 100, 'swing');

			$(canvas).faceDetection({
				//confidence: 0,
				complete: function(obj) {
					if (obj.length) {
						obj.sort((a, b) => b.confidence-a.confidence );
					}

					for (var j = 0; j < obj.length; j++) {
						context.strokeStyle = 'rgb(200, 0, 0)';
						context.strokeRect(obj[j].x, obj[j].y, obj[j].width, obj[j].height);
					}
					let opt = {
						aspectRatio: d_w/d_h
					};

					let x = mode.x/scale;
					let y = mode.y/scale;
					let x2 = x+mode.w/scale;
					let y2 = y+((mode.w/scale)*(d_h/d_w));

					context.strokeStyle = 'rgb(0, 200, 0)';
					context.strokeRect(x, y, x2-x, y2-y);

					if (obj.length) {
						x = obj[0].x+obj[0].width/2-(mode.w/scale)/2;
						y = obj[0].y+obj[0].height/2-((mode.w/scale)*(d_h/d_w))/2;
						x2 = x+mode.w/scale;
						y2 = y+((mode.w/scale)*(d_h/d_w));
					}

					if (x < mode.min_x/scale) {
						x = mode.min_x/scale;
						x2 = x+mode.w/scale;
					} else if (x > mode.max_x/scale) {
						x = mode.max_x/scale;
						x2 = x+mode.w/scale;
					}

					if (y < mode.min_y/scale) {
						y = mode.min_y/scale;
						y2 = y+((mode.w/scale)*(d_h/d_w));
					} else if (y > mode.max_y/scale) {
						y = mode.max_y/scale;
						y2 = y+((mode.w/scale)*(d_h/d_w));
					}

					opt['setSelect'] = [x, y, x2, y2];
					opt['onSelect'] = function(c) {
						let preview_canvas = document.getElementById('preview_'+$(canvas).attr("id").replace("canvas_", ''));
						let ctx = preview_canvas.getContext('2d');
						let img = new Image();
						img.onload = function() {
							ctx.drawImage(img, c.x*scale, c.y*scale, c.w*scale, c.h*scale, 0, 0, d_w, d_h);

							if ($(preview_canvas).next(".img_name").text() == '') {

								let callback_func = function (code) {
									$(preview_canvas).next(".img_name").text(code);
									let file_name = code+'.jpg';
									writeJpgFile(_path.join(dest_dir, dest, file_name), preview_canvas.toDataURL('image/jpeg'));
									$("#numerator").text(i+1);
									let v = parseInt((i+1)*100/_this.images.length);
									$('#progressbar').css('width', `${v}%`).text(`${v}%`);
									if (i+1 < _this.images.length) {
                    crop(i+1);
                  } else {
                    $('.function_btn').attr('disabled', false);
                  }
								};

								if (!mode.barcode_read) {
									callback_func(_path.basename(_this.images[i], _path.extname(_this.images[i])));
								} else {
									readBarcode(img).then(code => {
										if (code) {
											callback_func(code);
										} else {
											dialog.showErrorBox('Cannot read barcode!!', 'バーコード読み取りに失敗しました。');
											inputPrompt().then(code => {
												callback_func(code);
											});
										}
									});
								}
							} else {
								let file_name = $(preview_canvas).next(".img_name").text()+'.jpg';
								writeJpgFile(_path.join(dest_dir, dest, file_name), preview_canvas.toDataURL('image/jpeg'));
							}
						};
						img.src = image.src;
					};
					$(canvas).Jcrop(opt);
				}
			});
		}
		image.src = _this.images[i];

	}
	async function readBarcode(img) {
		let code = null;
    const theata_arr = [0, -1, 1, -2, 2];
		let states = [
			{decoder:{readers: [mode.barcode_type]}, inputStream: {size: 1280}, locator: {patchsize: 'medium'}},
			{decoder:{readers: [mode.barcode_type]}, inputStream: {size: 1920}, locator: {patchsize: 'medium'}},
			{decoder:{readers: [mode.barcode_type]}, inputStream: {size: 1280}, locator: {patchsize: 'large'}},
			{decoder:{readers: [mode.barcode_type]}, inputStream: {size: 1920}, locator: {patchsize: 'large'}},
			{decoder:{readers: [mode.barcode_type]}, inputStream: {size: 1600}, locator: {patchsize: 'medium'}},
			{decoder:{readers: [mode.barcode_type]}, inputStream: {size: 1600}, locator: {patchsize: 'medium'}}
		];

    let barcode_canvas = document.createElement('canvas');
    $(barcode_canvas).attr("width", mode.b_w);
    $(barcode_canvas).attr("height", mode.b_h);
    let barcode_ctx = barcode_canvas.getContext('2d');

    for (let i = 0; i < theata_arr.length; i ++) {
      barcode_ctx.clearRect(0, 0, mode.b_w, mode.b_h);
      barcode_ctx.save();
      barcode_ctx.rotate((theata_arr[i] * Math.PI) / 180);
      barcode_ctx.drawImage(img, mode.b_x, mode.b_y, mode.b_w, mode.b_h, 0, 0, mode.b_w, mode.b_h);
      barcode_ctx.restore();
      let src = barcode_canvas.toDataURL('image/jpeg');
      for (state of states) {
        state.src = src;
  			const result = await _decodeSingle(state);
  			if (typeof result === 'object' && typeof result.codeResult === 'object') {
  				return result.codeResult.code;
  			}
      }
    }

		return code;
	}

	function _decodeSingle(state) {
		return new Promise(resolve => {
			Quagga.decodeSingle(state, result => {
				resolve(result);
			});
		});
	}
  function writeJpgFile (path, data) {
    mkdirp(getDirName(path), function (err) {
      if (err) throw err;
      fs.writeFile(path, data.replace(/^data:image\/jpeg;base64,/, ''), 'base64', function (err) {if (err) throw err;});
    });
  }
  async function inputPrompt() {
    let r = '';
    while (!r || !/^\d+$/.test(r)) {
      r = await _prompt();
    }
    return r;
  }
  function _prompt() {
    return new Promise(resolve => {
      prompt({
        title: '番号入力',
        label: '番号を入力してください。',
        inputAttrs: {
          type: 'number'
        }
      }).then(r => {
        resolve(r);
      }).catch(console.error);
		});
  }
  function format_zero(num) {
    return `00${num}`.slice(-2);
  }
  </script>
</image-list>

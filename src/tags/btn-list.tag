<btn-list>
  <div class="position-absolute w-25" style="right: 0;bottom: 0;">
    <div class="card">
      <div class="card-body">
        <div class="form-group">
            <select class="form-control" name="mode" class="mode_select">
              <option each={ o, i in settings } value="{ i }">{ o.name }</option>
            </select>
        </div>
        <div class="form-group">
          <button class="btn btn-primary function_btn" onclick={ set_folder }>画像フォルダ</button>
        </div>
        <div class="form-group">
          <p class="text-center"><span id="numerator"></span>/<span id="denominator"></span></p>
          <div class="progress">
            <div id="progressbar" class="progress-bar progress-bar-striped bg-warning" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
          </div>
        </div>
        <div class="form-group">
          <input id="user_number" class="form-control" type="number" placeholder="管理番号">
        </div>
        <div class="form-group">
            <button class="btn btn-warning function_btn" onclick={ scroll_canvas }>移動</button>
            <button class="btn btn-secondary function_btn" onclick="location.reload();">リフレッシュ</button>
        </div>
      </div>
    </div>
  </div>
<script>
  const {BrowserWindow, dialog} = require('electron').remote;
  const fs = require('fs');
  const path = require('path');
  this.settings = require('./setting.js');

  set_folder(e) {
    const win = BrowserWindow.getFocusedWindow();
    dialog.showOpenDialog(
        win,
        {
          properties: ['openDirectory']
        },
        function (folder_path) {
          var photo_list = [];
          if (folder_path) {
            fs.readdir(folder_path[0], function (error, files) {
              if (error) throw error;
              files.filter(function (file) {
                return /.*\.je?pg$/i.test(file);
              }).forEach(function (file) {
                photo_list.push(path.join(folder_path[0], file));
              });
              obs.trigger('getPath', photo_list);
            });
          }

        });

  }
  scroll_canvas() {
    const input_num = $('#user_number').val();
    let is_exist = false;
    $('.img_name').each(function(i){
      if ($(this).text() == input_num) {
        $('html,body').animate({scrollTop: $(this).parent('td').offset().top}, 100, 'swing');
        is_exist = true;
        return false;
      }
    });
    if (!is_exist) {
      dialog.showErrorBox('Not Found!!', 'お探しの画像が見つかりませんでした。');
    }
  }
</script>
</btn-list>
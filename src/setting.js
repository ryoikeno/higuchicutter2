module.exports = [
	{name: '家電試験', barcode_read: false, barcode_type: 'code_39_reader', x: 150, min_x: 130, max_x: 170, y: 335, min_y: 325, max_y: 345, w: 660},
	{name: '家電更新', barcode_read: true, barcode_type: 'code_128_reader', x: 540, min_x: 528, max_x: 552, y: 100, min_y: 91, max_y: 109, b_x: 2000, b_y: 1300, b_w: 850, b_h: 500},
	{name: '消費生活郵送', canvas_max_w: 1200, barcode_read: false, x: 2130, min_x: 2125, max_x: 2135, y: 330, min_y: 325, max_y: 335, d_w: 192, d_h: 240, w: 140},
	{name: '消費生活手書き', canvas_max_w: 1200, barcode_read: false, x: 2120, min_x: 2115, max_x: 2125, y: 390, min_y: 385, max_y: 395, d_w: 192, d_h: 240, w: 135}
];